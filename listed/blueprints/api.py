from flask import Blueprint, render_template, abort, request, redirect, session, url_for
from flask.ext.login import current_user, login_user
from sqlalchemy import desc
from shutil import move
from datetime import datetime
from listed.objects import *
from listed.database import db
from listed.common import *
from listed.config import _cfg

import os
import zipfile
import urllib
import tempfile

api = Blueprint('api', __name__)

@api.route("/api/anime/title")
@json_output
def title():
    query = request.args.get('query')
    if not query:
        query = ''
    matches = db.query(AnimeTitle, Anime)\
        .filter(AnimeTitle.title.ilike('%' + query + '%'))\
        .filter(AnimeTitle.anime_id == Anime.id)\
        .limit(50)
    results = list(set([a for t, a in matches]))
    return [r.dumb_object() for r in results]
